<!-- This is a catch-all for discussions that fall out of existing templates -->

## Discussion Topic/Question
<!-- Describe the current behavior/state that makes this topic important to address -->

## Motivation and Context
<!-- How has this issue effected you? Provide context (e.g. examples, use
cases) -->

## Next Steps
<!-- Suggest a concrete way to explore this issue or a possible implementation -->

## External Resources
<!-- Images, links, code-snippets -->

--------------------------------------------------------------------------------

/label ~"discussion"

**Disclaimer:** This form is a work in progress. Feedback on its components is welcomed!
