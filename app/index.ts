// app/index.ts
import * as ReactDom from 'react-dom'
import * as h from 'react-hyperscript'
import {HashRouter, Route} from 'react-router-dom'
import App from './components/App'

ReactDom.render(
  h(HashRouter, [
    h(Route, {render: () => {return h(App)}})
  ]),
  document.getElementById('app')
)

