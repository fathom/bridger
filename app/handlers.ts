import {Handler} from '../lib/Handler'
import {GithubHandler, GithubMsg} from '../lib/Handlers/Github'
import {FathomHandler, FathomMsg} from '../lib/Handlers/fathom'
import {githubStatement, fathomStatement} from './statementGenerators'
import { ReactElement } from 'react'

export type MsgTypes = GithubMsg | FathomMsg

export const claimHandlers:{[props: string]: Handler<MsgTypes, any>} = {
  github: new GithubHandler(),
  fathom: new FathomHandler('http://localhost:8545', '0x7dc5d591cea09fb185891c935c9b26f568fb3d2b'),
}

export const claimStatements:{[props: string]: (msg: MsgTypes) => ReactElement<any>} = {
  github: githubStatement,
  fathom: fathomStatement
}

