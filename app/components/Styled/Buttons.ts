// app/components/Styled/Buttons.ts
import * as h from 'react-hyperscript'
import * as React from 'react'
import {NavLink} from 'react-router-dom'
// Components
import {StyledAddButton, StyledCancelButton, StyledViewSrcButton, StyledToggleViewButton} from './index'

// AddButton
export const AddButton: React.SFC<{}> = () => {
	return h(NavLink, {to: '/create'}, [h(StyledAddButton, '+')] )
}

// CancelButton
export const CancelButton: React.SFC<{}> = () => {
	return h(NavLink, {to: '/'}, [h(StyledCancelButton, 'x')] )
}

// // ViewSrcButton
interface IViewSrcButtonProps {hash: string}
export const ViewSrcButton: React.SFC<IViewSrcButtonProps> = (props) => {
  return h(NavLink, {to: `/claim/${props.hash}/src`}, [h(StyledViewSrcButton, '</>')] )
}

// ToggleViewButton
interface IToggleViewButtonProps {hash: string}
export const ToggleViewButton: React.SFC<IToggleViewButtonProps> = (props) => {
  return h(NavLink, {to: `/claim/${props.hash}`}, [h(StyledToggleViewButton, '< >')] )
}

// TODO:
// ViewButton
// CopyButton
// DeleteButton



