import styled from 'styled-components'

// App.ts
  // Header.ts => Logo, Search
  // Body.ts
    // AddButton.ts
    // ClaimList.ts => [ ClaimListItem ]
  // ClaimSelect.ts
  // ClaimForm.ts


// App.ts //-----------------------------
export const StyledOuterDiv = styled('div')`
  width: 100%;
  height: 100%;
`

// Header.ts //-----------------------------

export const StyledHeader = styled('div')`
  background-color: #292961;
  width: 100%;
  min-height 40px;
  border-bottom: 1px solid #e5e5e5;
  box-sizing: border-box;
  position: fixed;
`
export const StyledLogoType = styled('span')`
  padding: 4px;
  margin: 4px 12px;
  width: 10%;  
  font-size: 28px;
  color: white;
  display: inline-block;
`

// Search.ts


// Body.ts //-----------------------------
export const StyledBody = styled('div')`
  height: 100%;  
  background-color: gray;
  border: solid 1px gray;
  padding-top: 40px;
`
 
// ClaimList.ts,
// ClaimForm.ts //-----------------------------
export const StyledDiv = styled('div')`
  a:link {
    text-decoration: none;
  };
  background-color: Honeydew;
  min-height: 100%;
  text-align: center;
  font-family: Arial;
  padding: 2% 9%;
`

// ClaimListItem.ts,
// ClaimDetail.ts 
// ClaimSelect.ts //-----------------------------
export const StyledBoldType = styled('div')`
  font-weight: bold;
  font-size: 20px;
  padding: 2px 6px;
  color: #cc0000;
`
export const StyledBoldTypeBlack = StyledBoldType.extend`
  color: black;
`
export const StyledBoldTypeCenter = StyledBoldType.extend`
  text-align: center;
  font-size: 28px;
`
export const StyledBoldTypeCenterBlack = StyledBoldTypeCenter.extend`
  color: black;
`

export const StyledClaim = styled<any, any>('div')`
  border: 1px solid gray;
  border-radius: 15px;
  padding: 15px 25px;
  margin: 1.5% 0%;
  background-color: white;
  text-align: left;
  overflow-wrap: break-word;
  font-family: Arial;
`
export const StyledClaimPage = StyledClaim.extend`
  padding: 25px;
  margin: 5% 15%;
  overflow-wrap: break-word;
  font-family: Arial;
  font-size: 22px;
  background-color: white;
  position: relative;
`
export const StyledClaimSelect = StyledClaim.extend`
  width: 50%;
  text-align: center;
  display: inline-block; 
`

// ClaimListItem.ts //-----------------------------

export const StyledClaimStatement = StyledClaim.extend`
  padding: 10px 15px;
  margin: 1% 0%;
  font-size: 18px;
  border-width: 0.5px;
  border-radius: 25px;
  color: #00004d;
  background-color: #ffffdd;
`
export const StyledClaimStatementDetail = StyledClaimStatement.extend`
  font-size: 22px;
`

export const StyledClaimItemDiv = styled('div')`
  padding: 3px 6px;
  text-align: left;
`
export const StyledClaimLabel = styled('div')`
  font-size: 20px;
  padding: 2px 6px;
  color: #cc0000;
`
// export const StyledKeyword = styled('span')`
//   font-weight: bold;
// `
// export const StyledText = styled('span')`
//   font-weight: normal;
// `
export const StyledKey = styled('span')`
  font-weight: bold;
`
export const StyledVal = styled('span')`
  font-weight: normal;
`



// ClaimForm.ts //-----------------------------

export const StyledFormDiv = styled('div')`
  padding: 25px;
  margin: 5% 12%;
  border: 1px solid gray;
  border-radius: 6px;
  background-color: white;
  position: relative;
`
export const StyledFormTitle = styled('div')`
  font-family: Arial;
  font-size: 48px;
  margin-right: auto;
  margin-left: auto;
`
export const StyledSpacedDiv = styled('div')`
  padding: 10px;
`
export const StyledSpacedDivLeft = StyledSpacedDiv.extend`
  text-align: left;
`
export const StyledFormSubmit = styled('input')`
  border: 1px sollid gray;
  border-radius: 2px;
  background-color: #cce6ff;
  font-size: 14px;
`
export const StyledFormInput = styled<any, any>('input')`
  border-radius: 2px;
  margin-bottom: 5px;
  padding-left: 5px;
  width: 40%;
  display: inline-block;  
`

// Buttons.ts //-----------------------------
export const StyledAddButton = styled('div')`
	color: #292961;
	background-color: white;
	font-size: 28px;
	text-align: center;
	width: 34px;
	border-radius: 50%;
	border: solid 1px gray;
  position: fixed;
  top: 0;
  right: 0;
  margin: 60px 20px 40px 40px;
  cursor: pointer;
`

export const StyledCancelButton = StyledAddButton.extend`
  margin: 20px;
  position: absolute;
`

export const StyledViewSrcButton = StyledAddButton.extend`
  width: 68px;
  margin: 20px;
  border-radius: 50%;
  position: absolute;
  top: 0;
  right: 75px;
`

export const StyledToggleViewButton = StyledViewSrcButton.extend`
  width: 70px;
`

//-----------------------------



