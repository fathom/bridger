// app/components/Body/ClaimList/index.ts (ClaimList)

import * as React from 'react'
import * as h from 'react-hyperscript'
import {NavLink} from 'react-router-dom'
// Handlers
import {SignedMsg} from '../../../../lib/Handlers/SignedMessage'
// Components
import ClaimListItem from './ClaimListItem'
import {StyledDiv} from '../../Styled'
// import {orderClaims} from '../../../utils/claimUtils'
import {claimStatements} from '../../../handlers'

interface IClaimListProps {
	claimStore: {[hash: string]: SignedMsg}
}
const ClaimList: React.SFC<IClaimListProps> = (props) => {
	// make array; iterate over claimStore obj, make component for each claim and push it into array
	let claims = []
	for (let hash in props.claimStore) {
		let claimObj = props.claimStore[hash]
		let msgObj = claimObj.claim.data
		let claimStatement = claimStatements[claimObj.claim.type](msgObj)
		claims.push(h(NavLink, {to: '/claim/' + hash}, [
			h(ClaimListItem, {hash, claim: claimObj, claimStatement})
		]))
	}

	// TODO: Audit/fix this so claims are ordered chronologically
	let claimsRev = claims.reverse()

	return h('div', [
		h(StyledDiv, claimsRev)
	])
}
export default ClaimList
