// app/components/Body/View/ClaimListItem.ts (ClaimListItem)

// libraries
import * as React from 'react'
import {ReactElement} from 'react'
import * as h from 'react-hyperscript'
// utils
import {parseForClaimList, scrubJsonString, processClaimAttribute} from '../../../utils/claimUtils'
// components
import {StyledClaimLabel, StyledClaim, StyledClaimStatement, StyledClaimItemDiv, StyledKey, StyledVal} from '../../Styled'


interface IClaimListItemProps {
  hash: string,
  claim: any,
  claimStatement: React.ReactElement<any>
}

const ClaimListItem: React.SFC<IClaimListItemProps> = (props) => {
  let parsed = parseForClaimList(props.claim, props.hash) // => object
  let stringified = JSON.stringify(parsed) // => string
  let scrubbed = scrubJsonString(stringified) // => array (of strings)
  let formatted = scrubbed.map(attributeString => {
    let attributeTuple = processClaimAttribute(attributeString, true)
    return h(StyledClaimItemDiv, [
      h(StyledKey, attributeTuple[0]),
      h('span', ': '),
      h(StyledVal, attributeTuple[1])
    ])
  })

  let ClaimLabel = h(StyledClaimLabel, [
    h(StyledKey, props.claim.claim['type']),
    h(StyledVal, ' claim')
  ])

  return h(StyledClaim, {claim: props.claim}, [
    ClaimLabel,
    h(StyledClaimStatement, [props.claimStatement]),
    h('div', formatted)])
}

export default ClaimListItem


