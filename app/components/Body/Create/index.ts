// app/components/Body/Create/index.ts (ClaimSelect)

// Libraries
import * as React from 'react'
import {Route, Link, NavLink} from 'react-router-dom'
import * as h from 'react-hyperscript'
// Components
import ClaimForm from './ClaimForm'
import {StyledDiv, StyledClaimSelect} from '../../Styled'
import {CancelButton} from '../../Styled/Buttons'

// utils
import {claimHandlers, claimStatements} from '../../../handlers'
import { IMatch, IHistory } from '../../../utils/claimUtils'

interface IClaimSelectProps {
	address: string,
  sign: any,
  saveClaimToStore: any
}

const ClaimSelect: React.SFC<IClaimSelectProps> = (props) => {

	let claimTypes: string[] = Object.keys(claimHandlers)
	// iterate over claim types, create a link for each
	let claimSelectList = claimTypes.map((type, i) => {
		return h( StyledClaimSelect, {key: i}, [h(Link, {to: '/create/' + type}, type)] )
	})

  return h(StyledDiv, [
    h(Route, {
      exact: true,
      path: '/create',
      render: () => h('div', [
        h(CancelButton),
        h('h4', 'Select a claim type to create'),
        claimSelectList
      ])
    }),

    h(Route, {
      path: '/create/:type',
      render: ({ match, history }: {match: IMatch, history: IHistory}) => {
        return h(ClaimForm, {
          type: match.params.type,
          history,
          claimHandler: claimHandlers[match.params.type],
          claimStatement: claimStatements[match.params.type],
          address: props.address,
          sign: props.sign,
          saveClaimToStore: props.saveClaimToStore
        })
      }
    })

  ])
}

export default ClaimSelect
