// app/components/Body/Create/ClaimForm.ts (ClaimForm)

// libraries
import * as React from 'react'
import {Route} from 'react-router-dom'
import * as h from 'react-hyperscript'
const IpfsAPI = require('ipfs-api')
const ipfs = IpfsAPI('ipfs.infura.io', '5001', {protocol: 'https'})
// handlers
import {Handler} from '../../../../lib/Handler'
// components
import ClaimDetail from '../ClaimDetail'
import {StyledFormDiv, StyledFormTitle, StyledSpacedDiv} from '../../Styled'
import {CancelButton} from '../../Styled/Buttons'
// utils
import {createForm, onSubmitForm, deriveInputs, IMatch, IHistory, Iinputs} from '../../../utils/claimUtils'

export interface IClaimFormProps {
	type: string,
	history: IHistory,
  claimHandler: Handler<any, any>,
  claimStatement: (msg: any) => React.ReactElement<any>,
	address: string,
  sign: any,
  saveClaimToStore: any
}

const ClaimForm: React.SFC<IClaimFormProps> = (props) => {
  let inputs: Iinputs = deriveInputs(props.claimHandler)
  const handleChange = async (input: string, value: string) => {
    inputs[input].value = value
  }

  async function handleSubmit(e: Event) {
    e.preventDefault()
    
    let {type, history, claimHandler, claimStatement, address, sign, saveClaimToStore} = props

    // add a guard to catch any address mismatches 
    if (inputs.address.value.toLowerCase() !== address.toLowerCase()) {
      return console.log('Address in form does not match your signed-in address')
    }

    await onSubmitForm({type, history, claimHandler, claimStatement, address, sign, saveClaimToStore, inputs}, ipfs)
    return
  }

  let formFields = createForm(inputs, handleChange)

  return h(StyledFormDiv, [
    h(Route, {
      path: '/claim/:hash',
      render: ( {match}:IMatch ) => h(ClaimDetail, {
        hash: match.params.hash
      })
    }),
    h(CancelButton),
    h(StyledFormTitle, `${props.type} claim`),
    h(StyledSpacedDiv, `Enter and submit your claim info...`),
    h('form#' + props.type, { onSubmit: handleSubmit }, formFields)
  ])
}

export default ClaimForm



