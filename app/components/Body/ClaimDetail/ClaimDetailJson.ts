// app/components/Body/View/ClaimDetailJson.ts (ClaimDetailJson)

import * as h from 'react-hyperscript'
import * as React from 'react'
import {StyledClaimPage, StyledBoldType, StyledBoldTypeCenterBlack} from '../../Styled'
import {CancelButton, ToggleViewButton} from '../../Styled/Buttons'
import {claimHandlers} from '../../../handlers'
import {SignedMsg, SignedMessageHandler} from '../../../../lib/Handlers/SignedMessage'
import {parseForJsonView, indentJson, formatAttributes} from '../../../utils/claimUtils'

let signedHandler = new SignedMessageHandler

interface IClaimDetailJsonProps {
  hash: string,
  claimStore: {[hash: string]: SignedMsg}
}

export const ClaimDetailJson: React.SFC<IClaimDetailJsonProps> = (props) => {
  let claim = props.claimStore[props.hash]
  let parsedClaim = parseForJsonView(claim, props.hash)
  let formattedClaim = formatAttributes(parsedClaim)
  let jsonStringClaim = JSON.stringify(parsedClaim)
  let FormattedJson = indentJson(jsonStringClaim)

  let label = `${claim.claim.type} claim: `
  let subLabel = 'JSON source data'
  return h(StyledClaimPage, [
    h(CancelButton),
    h('div', [h(StyledBoldType, label), h(StyledBoldTypeCenterBlack, subLabel)]),
    h(ToggleViewButton, {hash: props.hash}),
    h('hr'),
    FormattedJson
  ])
}

