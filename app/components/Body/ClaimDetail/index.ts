// app/components/Body/View/ClaimDetail/index.ts (ClaimDetail)

import * as h from 'react-hyperscript'
import * as React from 'react'
import {Route} from 'react-router-dom'
import * as moment from 'moment'
import styled from 'styled-components'
// utils
import {parseForClaimDetail, formatAttributes, scrubJsonString, processClaimAttribute, IMatch} from '../../../utils/claimUtils'
// components
import {ClaimDetailJson} from './ClaimDetailJson'
import {StyledKey, StyledVal, StyledClaimPage, StyledBoldTypeCenter, StyledClaimStatementDetail, StyledClaimItemDiv} from '../../Styled'
import {CancelButton, ViewSrcButton} from '../../Styled/Buttons'
import {claimHandlers, claimStatements} from '../../../handlers'
import {SignedMsg, SignedMessageHandler} from '../../../../lib/Handlers/SignedMessage'

let signedHandler = new SignedMessageHandler


interface IClaimDetailProps {
  hash: string,
  claimStore: {[hash: string]: SignedMsg},
  match: IMatch
}
interface IClaimDetailState {
  claim: SignedMsg
  loaded: boolean
  error: string | null
}

class ClaimDetail extends React.Component<IClaimDetailProps, IClaimDetailState> {
  constructor (props: IClaimDetailProps){
    super(props)
    this.state = {
      loaded: false,
      claim: null,
      error: null
    }
  }

  async componentDidMount() {
    if(this.props.claimStore[this.props.hash]){
      let claim = this.props.claimStore[this.props.hash]
      return this.setState({claim, loaded:true})
    }

	  let ipfsData = await fetch('https://ipfs.infura.io/api/v0/cat/' + this.props.hash)
	  if (!ipfsData.ok) {
      return this.setState({loaded: true, error: "can't fetch hash"})
    }

	  let claimObj = JSON.parse(await ipfsData.text())
	  if (!signedHandler.isValid(claimObj)) {
      return this.setState({loaded: true, error: "malformed claim"})
    }

    let verifySignature = await signedHandler.verify(claimObj)
    if(verifySignature.valid === false){
      return this.setState({loaded:true, error: verifySignature.error})
    }

	  for (let handler in claimHandlers) { // {"github": githubHandler, ... }
		  if ( claimHandlers[handler].isValid(claimObj.claim.data) ) {
			  let verify = await claimHandlers[handler].verify(claimObj.claim.data)
			  if ( verify.valid === false) {
          return this.setState({loaded:true, error: verify.error})
			  }
        return this.setState({loaded:true, claim: claimObj})
		  }
	  }
    return this.setState({loaded: true, error: "unknown claimtype"})
  }

  render() {

    if (!this.state.loaded) {
      return h(StyledClaimPage, 'loading...')
    }

    if (this.state.error) {
      return h(StyledClaimPage, this.state.error)
    }

    let claim = this.state.claim.claim
    let msgObj = claim.data
    let claimStatement = claimStatements[claim.type](msgObj)
    let label = `${claim['type']} claim`

    let parsedClaim = parseForClaimDetail(this.state.claim, this.props.hash)
    let formattedClaim = formatAttributes(parsedClaim)

    let claimDataHeader = [h(StyledTypeCenter, 'claim form inputs:')]
    let formattedMsgObj = formatAttributes(msgObj)


    return h('div', [
      h(Route, {
        path: this.props.match.url + '/',
        exact: true,     
        render: () => h(StyledClaimPage, 
          {claim: this.state.claim}, [
          h(CancelButton),
          h(StyledBoldTypeCenter, label),
          h(ViewSrcButton, {hash: this.props.hash}),
          h(StyledClaimStatementDetail, [claimStatement]),
          h('div', formattedClaim),
          h(StyledInputsBox, claimDataHeader.concat(formattedMsgObj))
        ])
      }),

      h(Route, {
        path: this.props.match.url + '/src',
        render: () => h('div', [h(ClaimDetailJson, {
          hash: this.props.hash,
          claimStore: this.props.claimStore
        })])
      })

    ])
  }
}

export default ClaimDetail


const StyledKeyword = StyledKey.extend`
  color: #00004d;
`
const StyledKeyRedReg = StyledKey.extend`
  font-weight: normal;
  font-size: 24px;
  color: #cc0000;
`
const StyledInputsBox = StyledClaimStatementDetail.extend`
  border-radius: 15px
  background-color: #f0f0f0;
`
const StyledTypeCenter = styled('div')`
  text-align: center;
  font-weight: normal;
  font-size: 24px;
  padding: 2px 6px;
  color: black;  
`



