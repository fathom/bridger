// app/components/Body/index.ts (Body)

import * as h from 'react-hyperscript'
import * as React from 'react'
// Handlers
import {SignedMsg} from '../../../lib/Handlers/SignedMessage'
// Components
import ClaimList from './ClaimList'
import {StyledBody} from '../Styled'
import {AddButton} from '../Styled/Buttons'

interface IBodyProps {
  claimStore: {[hash: string]: SignedMsg}
}

const Body: React.SFC<IBodyProps> = (props) => {
  return h(StyledBody, [
    h(AddButton),
    h(ClaimList, {claimStore: props.claimStore})
  ])
}
export default Body
