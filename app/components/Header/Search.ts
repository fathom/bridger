// app/components/Header/Search.ts (Search)

import * as h from 'react-hyperscript'
import * as React from 'react'
import styled from 'styled-components'
import {isIpfsHashFormat} from '../../utils/claimUtils'

interface ISearchProps { history: any }
interface ISearchState { value: string, valid: boolean}

export default class Search extends React.Component<ISearchProps, ISearchState> {
	constructor(props: ISearchProps) {
		super(props)
		this.state = { value: '', valid: true }

		this.handleChange = this.handleChange.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
	}

  handleChange(e: Event) {
    let element = e.currentTarget as HTMLInputElement
    let value = element.value
    this.setState({
      value,
      valid: true
    })
  }

	async handleSubmit(e: Event) {
    e.preventDefault()
    if(!isIpfsHashFormat(this.state.value)) {
      return this.setState({valid:false})
    }
    return this.props.history.push('/claim/' + this.state.value)
	}

	render () {
		return h(SearchFormStyled, { onSubmit: this.handleSubmit }, [
		  h(StyledHeaderSearchInput, {
        valid: this.state.valid,
				type: 'search',
				value: this.state.value,
				placeholder: 'Search and verify claims...',
				onChange: this.handleChange,
			})
		])
	}
}

const SearchFormStyled = styled<any, any>('form')`
width: 50%;
display: inline-block;
`
interface StyledHeaderSearchInputProps {valid: boolean}
const StyledHeaderSearchInput = styled<any, any>('input')`
border-radius: 2px;
margin-bottom: 5px;
padding-left: 5px;
width: 80%;
background-color: AliceBlue;
display: inline-block;
border-color: ${(props: StyledHeaderSearchInputProps) => props.valid ? 'black' : 'red' }
`
