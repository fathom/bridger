// app/components/Header/index.ts (Header)

// Modules
import * as React from 'react'
import * as h from 'react-hyperscript'
// Components
import {StyledHeader, StyledLogoType} from '../Styled'
import Search from './Search'
import {IHistory} from '../../utils/claimUtils'

interface IHeaderProps { history: IHistory }

const Header: React.SFC<IHeaderProps> = (props) => {
	return h(StyledHeader, [
		h(StyledLogoType, 'bridger'),
		h(Search, {history: props.history})
	])
}
export default Header



