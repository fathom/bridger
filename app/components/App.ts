// app/components/App.ts

// Libraries
import * as React from 'react'
import * as h from 'react-hyperscript'
import {Route} from 'react-router-dom'
import Web3 from 'web3'

// Handlers
import {SignedMsg} from '../../lib/Handlers/SignedMessage'

// Components
import {StyledOuterDiv} from './Styled'
import Header from './Header'
import Body from './Body'
import Create from './Body/Create'
import ClaimDetail from './Body/ClaimDetail'

// Data, Utils
import {getClaimsFromStore, IMatch, IHistory} from '../utils/claimUtils'


interface IAppState {
  address: string,
  claimStore: {[hash: string]: SignedMsg}
}

export default class App extends React.Component<null, IAppState> {
	web3: any
  constructor (props: null) {
    super(props)

    this.state = {
      address: '',
      claimStore: {},
    }
    this.web3 = null

    // declare bindings
    this.sign = this.sign.bind(this)
    this.saveClaimToStore = this.saveClaimToStore.bind(this)
  }

  // set web3 object, address
  async componentDidMount () {
    let claimStore
    if (typeof (window as any)['web3'] !== 'undefined') {
      this.web3 = new Web3((window as any)['web3'].currentProvider)
      let address = (await this.web3.eth.getAccounts()
        .then(function(data: string[]) {
          return data[0].toLowerCase()
        })
        .catch((err: string) => console.log(err))
      )
      if (!address) return window.alert('Please unlock metamask')
      claimStore = getClaimsFromStore(address)

      this.setState({ address, claimStore })
    }
  }

  sign (text: string) {
    if (!this.state.address) {
      return window.alert('Please unlock metamask')
    }
    return this.web3.eth.personal.sign(text, this.state.address)
  }

  saveClaimToStore (signedClaim:SignedMsg, inputHash:string, address:string) {
    let claimStore = Object.assign({}, this.state.claimStore)
    if (!claimStore[inputHash]) {
      claimStore[inputHash] = signedClaim
      this.setState({claimStore})
      localStorage.setItem(`claimStore_${address}`, JSON.stringify(this.state.claimStore))
    }
    // TODO: handle case where claim is already in store
    return true
  }

  render () {

    return h(StyledOuterDiv, [

    		h(Route, {
    			exact: true,
    			path: '/',
          render: ( {history}: IHistory ) => {
            return h('div', [ h(Header, { history }), h(Body, {claimStore: this.state.claimStore})] )
    		  }
        }),

    		h(Route, {
    			path: '/create',
    			render: ( {history}: IHistory ) => h(Create,
            {
              address: this.state.address,
              sign: this.sign,
              saveClaimToStore: this.saveClaimToStore
            }
          )
    		}),

        h(Route, {
          path: '/claim/:hash',
          render: ( {match}: IMatch ) => h(ClaimDetail, {
            match,
            hash: match.params.hash,
            claimStore: this.state.claimStore
          })
        })

    ])
  }
}





