// app/statementGenerators.index.ts

import * as h from 'react-hyperscript'
import { ReactElement } from 'react'
import {FathomMsg} from '../../lib/Handlers/fathom/index'
import {GithubMsg} from '../../lib/Handlers/Github'
import {abbreviateString} from '../utils/claimUtils'//
import {StyledKey, StyledVal} from '../components/Styled/index'

// each statementGenerator: returns h component containing its claim statement
export const fathomStatement = (msg: FathomMsg):ReactElement<any> => {
	let Statement: ReactElement<any>
	if (msg.type === 'assessor' || msg.type === 'assessee') {
		// statement = `${msg.address} is an ${msg.type} on fathom assessment ${msg.assessment}`
		Statement = h('div', [
			h(StyledVal, abbreviateString(msg.address)),
			h(StyledVal, ' is an '),
			h(StyledKey, msg.type),
			h(StyledVal, ' on '),
			h(StyledKey, 'fathom assessment '),
			h(StyledVal, abbreviateString(msg.assessment))
		])
	}
	else if (msg.type === 'credential') {
		// Statement = `${msg.address} holds a fathom ${msg.type} from assessment ${msg.assessment}`
	  Statement = h('div', [
			h(StyledVal, abbreviateString(msg.address)),
			h(StyledVal, ' holds a '),
			h(StyledKey, `fathom ${msg.type}`),
			h(StyledVal, ' from assessment '),
			h(StyledVal, abbreviateString(msg.assessment))
	  ])
	}
	return Statement
}

export const githubStatement = (msg: GithubMsg):ReactElement<any> => {
	// let statement: string = `${msg.address} is ${msg.username} on github`
	let Statement = h('div', [
		h(StyledVal, abbreviateString(msg.address)),
		h(StyledVal, ' is '),
		h(StyledKey, msg.username),
		h(StyledVal, ' on '),
		h(StyledKey, 'github')
	])
	return Statement	
}
