// app/utils/claimUtils.ts

// libraries
import { ReactElement } from 'react'
import * as h from 'react-hyperscript'
import * as moment from 'moment'
import {SignedMsg} from '../../lib/Handlers/SignedMessage'
import styled from 'styled-components'

// handlers
import {Handler, Result} from '../../lib/Handler'
import {MsgTypes} from '../handlers'

// components
import {StyledFormSubmit, StyledSpacedDivLeft, StyledFormInput, StyledClaimItemDiv, StyledKey, StyledVal } from '../components/Styled/index'

// imported interfaces
import {IClaimFormProps} from '../components/Body/Create/ClaimForm'


//-----------------------------------------------------------------

// INTERFACES 
export interface IMatch {[key: string]: any}
export interface IHistory {[key: string]: any}
export interface Iinputs {[props: string]: {description: string, value: string}}
interface IonSubmitForm extends IClaimFormProps {inputs: Iinputs}

// HELPER FUNCTIONS 
//-----------------------------------------------------------------
// App.ts
export const getClaimsFromStore = (address: string) => {
  let claimStore = {}
  // reference those particular claims as a subset of claimStore, by appending the address
  let localClaimsByAddress = localStorage.getItem("claimStore" + "_" + address)
  if (localClaimsByAddress) {
    try { claimStore = JSON.parse(localClaimsByAddress) }
    catch(e) {console.log(e)}
  }
  return claimStore
}

//-----------------------------------------------------------------
// Body/Create/ClaimForm.ts

export const createForm = (inputs: Iinputs, handleChange: (input: string, value: string) => void) => {
  let formFields = []
  
  formFields.push(
  	h(StyledFormSubmit, {type: 'submit', value: 'submit'})
  )
  // populate formFields
  for (let input in inputs) {
    formFields.unshift(
      h(StyledSpacedDivLeft, [
        h('span', inputs[input].description + ': '),
        h(StyledFormInput, {
          className: 'input#' + input,
          onChange: (e: Event) => {
            let element = e.currentTarget as HTMLInputElement
            let value = element.value
            handleChange(input, value)
          }
        })
      ])
    )
  }
  return formFields
}

export const deriveInputs = (handler: Handler<any, any>):Iinputs | null => {
  if (!handler) return null
  let inputs: Iinputs = {}
  for (let key in handler.description) {
    inputs[key] = {
      value: '',
      description: handler.description[key].description
    }
  }
  return inputs
}

export const onSubmitForm = async ({type, history, claimHandler, claimStatement, address, sign, saveClaimToStore, inputs}: IonSubmitForm, ipfs: {[key: string]: any}) => {
  let data: any = {}
  // initialize claimInstance object 
  let claimInstance = {
    type,
    timestamp: Date.now(),
    data
  }
  // capture user inputs into claimInstance object
  for (let key in inputs) {
    claimInstance.data[key] = inputs[key].value
  }

  const statement = claimStatement(claimInstance.data)

  // VERIFY
  let verifyResult: {valid: boolean, error?: string} = await claimHandler.verify(claimInstance.data)
  if (verifyResult.valid) {
    let signature = await (sign(JSON.stringify(claimInstance)))
    let signedObj = {claim: claimInstance, signature, address}

    // save object to ipfs, get its ipfs hash
    console.time('ipfs-hash-creation')
    let hash = (await ipfs.files.add(Buffer.from(JSON.stringify(signedObj))))[0].hash
    console.timeEnd('ipfs-hash-creation')

    // TODO ?: pass statement (as h component) to ClaimListItem and ClaimDetail in 'components/Body/View...'

    saveClaimToStore(signedObj, hash, address)

    // if hash value exists, push it to history
    if (hash) {
      history.push('/claim/' + hash)
    }
  } else {
    console.log(verifyResult.error)
  }
}

export const isIpfsHashFormat = (input: string) => {
  return ((input.slice(0, 2) === 'Qm') && (input.length === 46))
}

//-----------------------------------------------------------------
// Body/ClaimList.ts

// export const orderClaims = (claims: {[props: string]: any}) => {
//   let hashes = Object.keys(claims)
//   return hashes
//     .map(hash => Object.assign({hash: hash}, claims[hash]))
//     // TODO: audit the logic in this function, as sorting becomes complicated once data/functions are wrapped in React packaging
//     .sort((claimA, claimB) => claimB.claim.timestamp - claimA.claim.timestamp)
// }


//-----------------------------------------------------------------
// ClaimListItem

export const parseForClaimDetail = (signedClaim: SignedMsg, hash: string) => {

  if (signedClaim.claim && hash) {
    return {
      type: signedClaim.claim.type,
      timestamp: signedClaim.claim.timestamp,
      signature: signedClaim.signature,
      hash
    }
  } else return {}
}

export const parseForClaimList = (signedClaim: SignedMsg, hash: string) => {
  if (signedClaim.claim) {
    return {
      address: signedClaim.claim.data.address,
      timestamp: signedClaim.claim.timestamp,
      hash
    }
  } else return {}
}

export const parseForJsonView = (signedClaim: SignedMsg, hash: string) => {
  if (signedClaim.claim && hash) {
    return {
      type: signedClaim.claim.type,
      timestamp: signedClaim.claim.timestamp,
      data: signedClaim.claim.data,
      signature: signedClaim.signature,
      hash
    }
  } else return {}  
}

export const formatAttributes = ( msg: {[key:string]:any} ):ReactElement<any>[] => {
  return Object.keys(msg).map(key => {
    return h(StyledClaimItemDiv, [
      h(StyledKey, key),
      h('span', ': '),
      h(StyledVal, msg[key])
    ])
  })
}

export const scrubJsonString = (jsonString:string):string[] => {
  return jsonString
    .replace(/[{}]/g,'')
    .replace(/\"/g, '')
    .split(',') 
}

export const abbreviateString = (longVal:string):string => {
  let len = longVal.length
  return (len > 10) ? (`${longVal.substring(0, 5)}...${longVal.substring(len-3, len)}`) : longVal
}

export const processClaimAttribute = (claimAttribute: string, abbreviate: boolean):[string, any] => {
  let [key, val] = claimAttribute.split(':')
  if (key === 'timestamp') {
    key = 'created'
    val = moment(parseInt(val)).format('lll')
  }
  if (key === 'type') {
    key = 'claim type'
  }
  if (key === 'address' || key === 'hash' && abbreviate === true) {
    val = abbreviateString(val)
  }
  return [key, val]
}


export const indentJson = (jsonString: string):ReactElement<any> => {
  let i, j=0, 
      len = jsonString.length, 
      tempStr = '',
      result = []

  for (i = 0; i < len; i++) {
    if (jsonString[i] === '"') {
      continue
    }
    else if (jsonString[i] === '{') {
      result.push(h(StyledIndentedDiv, {indentation: j}, tempStr))
      result.push(h(StyledIndentedDiv, {indentation: j}, (indentBrackets(j) + '{')))
      j+=20
      tempStr = ''
    }
    else if (jsonString[i] === ',') {
      tempStr += jsonString[i]
      result.push(h(StyledIndentedDiv, {indentation: j}, tempStr))
      tempStr = ''
    }
    else if (jsonString[i] === '}') {
      result.push(h(StyledIndentedDiv, {indentation: j}, tempStr))
      j-=20
      if (jsonString[i+1] === ',') {
        result.push(h(StyledIndentedDiv, {indentation: j}, (indentBrackets(j) + '},')))
        i++
      } else {
        result.push(h(StyledIndentedDiv, {indentation: j}, (indentBrackets(j) + '}')))
      }
      tempStr = ''
    }
    else {
      tempStr+= jsonString[i] 
    }
  }

  return h('div', result)
}

const indentBrackets = (num: number):string => {
  let result = []
  for (let i = 0; i < num; i++) {
    result.push(' ')
  }
  return result.join('')
}

interface IStyledIndentedDivProps {indentation: number}
const StyledIndentedDiv = styled('div')`
  padding-left: ${(props: IStyledIndentedDivProps) => props.indentation}px;
`

//-----------------------------------------------------------------








