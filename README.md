# What is it?
Bridger is a library and CLI for creating and verifying claims about Ethereum
identities.

You can read about the motivations behind it [here](https://gitlab.com/fathom/org/issues/2)

These claims are currently defined in `ClaimsTypes.js`, but eventually will be
defined individually in this repo. New claim types can be added by opening a MR. 

Each claim defines it's inputs as well as the code for validiting them, and
returns a boolean.

# Usage
First run `npm i` and `npm run build`

You can interact with the cli file directly (i.e `./cli.js` from the root
director) or symlink it to your path as `bridger` using `npm link`.

Currently the only available claim type is `github` and so to generate a claim call
`./cli.js gen github`

Run `./cli.js --help` to see all commands

# Vision
The goal is for Bridger to consist of a core library and many views onto it, a 
CLI, a Web-App and maybe even others, in order to enable a simple and secure 
ecosystem to talk about claims and aspects around Ethereum identities.

## How is it different from OpenBadges/Blockcerts/UPort
The goal of bridger is not to enable identities to make claims about other 
identites (i.e a school issues a claim/certificate about a student) but to be a 
generalized framework for talking about data-backed claims. Those kinds of claims 
_could_ be a subset of bridger, a ClaimType of their own, but are not the main 
focus. 

Our goal immediate goal is to enable identities to interact meaningfull around
blockchain and distributed data, by allowing them to bridge different facets of 
themselves easily and securely.

