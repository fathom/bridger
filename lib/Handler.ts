export type Result<Errors> = {
  valid: true
} | {
  valid: false
  error: Errors
}

export abstract class Handler<MsgType, Errors> { 
  /** A description of the Message Type */
  abstract description: {
    [prop in keyof MsgType]: {
      description: string,
      type?: string
    }
  }

  abstract async verify(msg: MsgType): Promise<Result<Errors>>
  
  abstract isValid(msg: any): msg is MsgType
}

export default { Handler }
