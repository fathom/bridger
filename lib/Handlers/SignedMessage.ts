// lib/Handlers/SignedMessage.ts

import { Handler, Result } from '../Handler'
const ethSigUtil = require('eth-sig-util')

export interface SignedMsg {
  signature: string,
  claim: any,
  address: string
}

export type Errors = "Wrong Address" | "Invalid Signature"

export class SignedMessageHandler extends Handler<SignedMsg, Errors>{  

  description = {
    signature: {
      description: "a signature of the message signed with your address"
    },
    claim: {description: "The message you are signing"},
    address: {description: "The address you are signing with"}
  }

  async verify (msg:SignedMsg): Promise<Result<Errors>> {
    console.log('SignedMessageHandler.verify -- msg: ', msg)
    // create msgParams obj from signedClaim
    let msgParams = {
      data: '0x' + (Buffer.from(JSON.stringify(msg.claim), 'utf8')).toString('hex'),
      sig: msg.signature
    }
    // look up user account/address using ethSigUtil library
    let address

    try {
      address = ethSigUtil.recoverPersonalSignature(msgParams)
    }
    catch(e){
      return { valid: false, error: "Invalid Signature"}
    }

    if (address !== msg.address) {
      return { valid: false, error: "Wrong Address"}
    }

    return {valid: true}
  }

  isValid(msg: any): msg is SignedMsg {
    console.log('SignedMessageHandler.isValid -- msg: ', msg)
    return (msg.signature !== undefined && msg.claim !== undefined && msg.address !== undefined)
  }
}
