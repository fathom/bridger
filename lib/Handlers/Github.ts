import { Handler, Result } from '../Handler'
const fetch = require('cross-fetch')

const regex = /^https:\/\/gist\.github\.com\/([\w]+)\/([\w]{32})\/?$/g

export interface GithubMsg {
  username: string,
  gistURL: string,
  address: string,
}

export type Errors = 'NoGistUrl' | 'NoGist' | 'NoUsername' | 'UsernameDoesNotMatchGist' | 'AddressDoesNotMatchGist'

export class GithubHandler extends Handler<GithubMsg, Errors>{
  description = {
    username: {
      description: "Your github username"
    },
    gistURL: {
      description: "A URL of a github gist containing your ETH address",
    },
    address: {
      description: "Your ETH Address"
    }
  }

  async verify(msg: GithubMsg): Promise<Result<Errors>> {
    let values = regex.exec(msg.gistURL)
    let username = msg.username

    if (values === null) {
      return {
        valid: false,
        error: "NoGistUrl"
      }
    }

    let gistUser = values[1]
    let gistID = values[2]

    let githubAPIRequest = await fetch('https://api.github.com/gists/' + gistID)
    if (!githubAPIRequest.ok) {
      return {
        valid: false,
        error: "NoGist"
      }
    }

    let gist = await githubAPIRequest.json()
    if (gist.owner.login !== gistUser) {
      return {
        valid: false,
        error: "NoGist"
      }
    }

    if (!username) {
      return {
        valid: false,
        error: 'NoUsername'
      }
    }

    if (gistUser !== username) {
      return {
        valid: false,
        error: 'UsernameDoesNotMatchGist'
      }
    }

    let gistAddress:string = gist.files[Object.keys(gist.files)[0]].content

    if (gistAddress.toLowerCase() !== msg.address.toLowerCase()){
      return {
        valid: false,
        error: 'AddressDoesNotMatchGist'
      }
    }

    return {valid: true}
  }

  isValid(msg: any): msg is GithubMsg{
    return (msg.username !== undefined && msg.gistURL !== undefined && msg.address !== undefined)
  }
}
