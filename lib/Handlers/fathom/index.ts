import { Handler, Result } from '../../Handler'

const Eth = require('ethjs')

const ConceptRegistryABI = require('./contracts/ConceptRegistry.js')
const AssessmentABI = require('./contracts/Assessment.js')
const ConceptABI = require('./contracts/Concept.js')

export interface FathomMsg {
  type: "assessor" | "assessee" | "credential",
  assessment: string,
  address: string,
}

type Errors =
  "invalid Concept" |
  "invalid Assessment" |
  "not assessee" |
  "not assessor" |
  "wrong type"
export class FathomHandler extends Handler<FathomMsg, Errors>{
  eth: any
  conceptRegistry: any
  description = {
    type: {
      description: "Assessor, assessee, or credential"
    },
    assessment: {
      description: "The address of the assessment"
    },
    address: {description: "The address of the user"}
  }

  constructor(ProviderURL: string, ConceptRegistry: string) {
    super()
    this.eth = new Eth(new Eth.HttpProvider(ProviderURL))
    this.conceptRegistry = this.eth.contract(ConceptRegistryABI).at(ConceptRegistry)
  }

  async verify(msg: FathomMsg): Promise<Result<Errors>> {
    let assessment = this.eth.contract(AssessmentABI).at(msg.assessment)
    let conceptAddress = (await assessment.concept())[0]

    if (!(await this.conceptRegistry.conceptExists(conceptAddress))) {
      return { valid: false, error: "invalid Concept" }
    }

    let concept = this.eth.contract(ConceptABI).at(conceptAddress)
    if (!(await concept.assessmentExists(msg.assessment))) {
      return { valid: false, error: "invalid Assessment"}
    }

    switch(msg.type) {
      case 'assessee':
        if((await assessment.assessee())[0] !== msg.address) {
          return {valid: false, error: "not assessee"}
        }
        return {valid: true}
      case 'assessor':
        if(!((await assessment.assessorState(msg.address))[0] > 1)){
          return {valid: false, error: "not assessor"}
        }
        return {valid: true}
      default:
        return {valid: false, error: "wrong type"}
    }
  }

  isValid (msg: any): msg is FathomMsg {
    return (msg.assessment !== undefined &&
            msg.address !== undefined && (
            msg.type === "assessor" || msg.type === 'assessee' || msg.type === 'credential'))
  }
}
