module.exports = [
  {
    'constant': true,
    'inputs': [],
    'name': 'fathomToken',
    'outputs': [
      {
        'name': '',
        'type': 'address'
      }
    ],
    'payable': false,
    'stateMutability': 'view',
    'type': 'function'
  },
  {
    'constant': false,
    'inputs': [
      {
        'name': 'parentList',
        'type': 'address[]'
      },
      {
        'name': '_propagationRates',
        'type': 'uint256[]'
      },
      {
        'name': '_lifetime',
        'type': 'uint256'
      },
      {
        'name': '_data',
        'type': 'bytes'
      },
      {
        'name': '_owner',
        'type': 'address'
      }
    ],
    'name': 'makeConcept',
    'outputs': [
      {
        'name': '',
        'type': 'address'
      }
    ],
    'payable': false,
    'stateMutability': 'nonpayable',
    'type': 'function'
  },
  {
    'constant': true,
    'inputs': [],
    'name': 'mewAddress',
    'outputs': [
      {
        'name': '',
        'type': 'address'
      }
    ],
    'payable': false,
    'stateMutability': 'view',
    'type': 'function'
  },
  {
    'constant': true,
    'inputs': [],
    'name': 'distributorAddress',
    'outputs': [
      {
        'name': '',
        'type': 'address'
      }
    ],
    'payable': false,
    'stateMutability': 'view',
    'type': 'function'
  },
  {
    'constant': true,
    'inputs': [
      {
        'name': '',
        'type': 'address'
      }
    ],
    'name': 'conceptExists',
    'outputs': [
      {
        'name': '',
        'type': 'bool'
      }
    ],
    'payable': false,
    'stateMutability': 'view',
    'type': 'function'
  },
  {
    'constant': false,
    'inputs': [
      {
        'name': '_token',
        'type': 'address'
      },
      {
        'name': '_distributor',
        'type': 'address'
      }
    ],
    'name': 'init',
    'outputs': [],
    'payable': false,
    'stateMutability': 'nonpayable',
    'type': 'function'
  },
  {
    'anonymous': false,
    'inputs': [
      {
        'indexed': false,
        'name': '_concept',
        'type': 'address'
      }
    ],
    'name': 'ConceptCreation',
    'type': 'event'
  }
]
