const path = require('path')

module.exports = {
  mode: 'development',
  entry: './app/index.ts',
  devServer: {
    contentBase: './public'
  },
  devtool: 'inline-source-map',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'public')
  },
  // tell webpack to support `.ts` files
  resolve: {extensions: ['.ts', '.js']},
  // tell webpack to use `ts-loader` (TypeScript loader for webpack) for all `.ts` files
  module: {rules: [{test: /\.ts$/, loader: 'ts-loader'}]}
}
